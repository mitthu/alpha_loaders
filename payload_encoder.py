#!/usr/bin/python
import sys

alpha_bytes = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
encoding_bytes = "0123456789abcdefghijklmnopqrstuvwxABCDEFGHIJKLMNOPQRSTUVWXYZ" # No 'y' and 'z'
end_of_stream = ""

# Scheme 2: Alpha Freedom (af_)
af_unpermitted_alpha_bytes = "zKLMNOPQRSTUVWXYZ"
af_encoding_bytes = "KLMNOPQRSTUVWXYZ"

# Accepts a byte: 'b'
def jan_weaver_encode(b):
	control = 'y'
	first = ''  # EF
	second = '' # CD

	# Calculating the first byte
	f = ord(b) & 0b00001111
	for x in encoding_bytes:
		tmp = ord(x) & 0b11110000
		tmp = tmp | f
		if chr(tmp) in encoding_bytes:
			first = chr(tmp)
			break

	# Calculating the second byte
	s = (ord(b) >> 4) ^ (ord(first) >> 4)
	s = s & 0b00001111
	for x in encoding_bytes:
		tmp = ord(x) & 0b11110000
		tmp = tmp | s
		if chr(tmp) in encoding_bytes:
			second = chr(tmp)
			break
	return control + second + first

# Accepts the return value of jan_weaver_encode
# Used for debugging and testing purpose only.
def jan_weaver_decode(s):
	CD = ord(s[1]) & 0b00001111
	EF = ord(s[2])
	return chr((CD << 4) ^ EF)

def encode_byte(b):
	if b in alpha_bytes:
		if b in 'yz':
			write_stream.write(jan_weaver_encode(b))
		else:
			write_stream.write(b)
	else:
		# 'b' is not alpha.
		write_stream.write(jan_weaver_encode(b))

def decode_byte(b):
	if b == 'z':
		sys.exit()
	elif b == 'y':
		tmp = 'y' + read_stream.read(2)
		write_stream.write(jan_weaver_decode(tmp))
	else:
		write_stream.write(b)

# Accepts a byte: 'b'
def af_jan_weaver_encode(b):
	first = ''  # EF
	second = '' # CD

	# Calculating the first byte
	f = ord(b) & 0b00001111
	for x in af_encoding_bytes:
		tmp = ord(x) & 0b11110000
		tmp = tmp | f
		if chr(tmp) in af_encoding_bytes:
			first = chr(tmp)
			break
	# Calculating the second byte
	s = (ord(b) >> 4) ^ (ord(first) >> 4)
	s = s & 0b00001111
	for x in af_encoding_bytes:
		tmp = ord(x) & 0b11110000
		tmp = tmp | s
		if chr(tmp) in af_encoding_bytes:
			second = chr(tmp)
			break
	return second + first

# Accepts the return value of jan_weaver_encode
# Used for debugging and testing purpose only.
def af_jan_weaver_decode(s):
	CD = ord(s[0]) & 0b00001111
	EF = ord(s[1])
	return chr((CD << 4) ^ EF)

def af_encode_byte(b):
	if b in alpha_bytes:
		if b in af_unpermitted_alpha_bytes:
			write_stream.write(af_jan_weaver_encode(b))
		else:
			write_stream.write(b)
	else:
		# 'b' is not alpha.
		write_stream.write(af_jan_weaver_encode(b))

def af_decode_byte(b):
	if b in af_encoding_bytes:
		tmp = b + read_stream.read(1)
		write_stream.write(af_jan_weaver_decode(tmp))
	else:
		write_stream.write(b)

# Processing of input starts here.
read_stream = sys.stdin
write_stream = sys.stdout
error_stream = sys.stderr

# Setup the loader.
LOADERS = {
	"nat": {
		'name': "Non-Alpha Touch",
		'encode': encode_byte,
		'decode': decode_byte,
		'end_mark': 'z',
		'closing_mark': 's'
	},
	"af": {
		'name': "Alpha Freedom",
		'encode': af_encode_byte,
		'decode': af_decode_byte,
		'end_mark': 'z',
		'closing_mark': 's'
	}
}
default_loader = "nat"

# Allowing extra bytes, other than alpha-numeric.
if '-a' in sys.argv or '--allow-alpha' in sys.argv:
	try:
		index = sys.argv.index('-a')
	except:
		index = sys.argv.index('--allow-alpha')
	alpha_bytes += sys.argv[index+1]

loader = default_loader
if '-l' in sys.argv or '--loader' in sys.argv:
	try:
		index = sys.argv.index('-l')
	except:
		index = sys.argv.index('--loader')

	try:
		loader = sys.argv[index+1]
	except:
		error_stream.write("No loader specified. Falling back to default loader (%s).\n" % loader)
		loader = default_loader
	if loader not in LOADERS:
		error_stream.write("Invalid loader specified. Falling back to default loader (%s).\n" % loader);
		loader = default_loader

"""
# Valid Process functions:
 - decode_byte
 - af_decode_byte
Valid end_mark:
 - z
"""
def decode_loop(process_function, end_mark):
	global end_of_stream
	end_of_stream += end_mark

	raw = read_stream.read(1)
	while raw not in end_of_stream:
		process_function(raw)
		raw = read_stream.read(1)

"""
# Valid Process functions:
 - encode_byte
 - af_encode_byte
Valid closing_mark:
 - z
"""
def encode_loop(process_function, closing_mark):
	raw = read_stream.read(1)
	while True:
		if raw != '':
			process_function(raw)
			raw = read_stream.read(1)
		else:
			write_stream.write(closing_mark)
			break

# Decoding happens if appropriate flags are passed.
if '-d' in sys.argv or '--decode' in sys.argv:
	decode_loop(LOADERS[loader]['decode'], LOADERS[loader]['end_mark'])
	sys.exit()

encode_loop(LOADERS[loader]['encode'], LOADERS[loader]['closing_mark'])
