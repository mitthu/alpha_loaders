.text						# start of code indicator.
.globl looped_encoder		# make the main function visible to the outside.
looped_encoder:				# actually label this spot as the start of our main function.
    call    anchor
anchor:
    # ebp contains the absolute address of start of looped_encoder
    popl    %ebx
    # $52 should always be the size of this header
    addl	$52, %ebx    # ebx = start of payload

    # Initialize get (ecx) and put (edx) registers
    movl    %ebx, %ecx
    movl    %ecx, %edx
    # At this point,
    # get (ecx) = put (edx) = start of payload

loop:
    movl    $-121, %eax
    addb    (%ecx), %al        # eax = -121 + [get]
    jl  alpha_skip
    jl  non_alpha_combine
    jg  end_of_payload

alpha_skip:
    # eax becomes tmp storage
    movb    (%ecx), %al        # al = b1
    movb    %al, (%edx)        # put (write) b1 to the write location.

    # increment the read and write pointers
    inc %ecx
    inc %edx

    jmp loop

non_alpha_combine:
    # eax becomes tmp storage
    # Skip the marker 'y'
    inc %ecx
    movb    (%ecx), %al     # al = CD
    shlb    $4, %al         # CD << 4
    inc %ecx
    xorb    (%ecx), %al     # (CD << 4) ^ EF
    inc %ecx

    movb    %al, (%edx)     # al -> [put]
    inc %edx                # put++

    jmp loop

end_of_payload:
    # NULL terminate the payload
    movb    $0, (%edx)
