#############################################################################
##                                                                         ##
## cpu_any.py --- base classes, architecture independant functions         ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: cpu_any.py,v 1.5 2004/04/30 10:12:48 pbi Exp $

import os,re

from sfcommon import *


class Loaders:
    def __init__(self, **opts):
        for i in opts:
            if i not in self.options:
                raise TypeError("Unknown option [%s]" % i)
        self.opts = opts
        self.loaders = filter(lambda x: x.startswith("loader_"),
                              self.__class__.__dict__)
    def update_options(self, **opts):
        self.opts.update(opts)
    def delete_option(self, option_name):
        del(self.opts[option_name])
    
        

class CodeTuner:
    def __init__(self):
        pass
    def tune_code(self, shcode):
        return shcode
    def automaton_error(self):
        error("Automaton failed. Complain at <phil@secdev.org>.\n"+
              "Join your C file and the full output (stdout and stderr)\n"+
              "of shellforge with -v5.")
        
    
class Binutils:
    def __init__(self, arch, root, CC="gcc", OBJDUMP="objdump", CFLAGS="", LDFLAGS=""):
        self.CC = CC
        self.OBJDUMP = OBJDUMP
#        self.CFLAGS = "-O3 -S -fPIC -Winline -finline-functions -ffreestanding -fomit-frame-pointer -fno-zero-initialized-in-bss"+CFLAGS
        self.CFLAGS = "-O3 -S -fPIC -Winline -finline-functions -ffreestanding -fomit-frame-pointer"+CFLAGS
        self.INCLUDES = "-isystem '%s' -isystem '%s'" % (root, "%s/%s" %(root,arch))
        self.INCLUDES += " -include %s/%s/sflib.h" % (root,arch)
        self.LDFLAGS = ""
        self.ASSEMBLEFLAGS = "-c"

    def show_versions(self):
        printinfo(5,"Machine :")
        os.system("uname -a 1>&2")
        printinfo(5,"Compiler : [%s]" % self.CC)
        os.system("%s --version 1>&2" % self.CC)
        printinfo(5,"objdump  : [%s]" % self.OBJDUMP)
        os.system("%s --version 1>&2" % self.OBJDUMP)

    def compile(self, file):
        printinfo(1, "** Compiling %s" % file)
        cmd = "%s %s %s -o - %s" % (self.CC, self.CFLAGS, self.INCLUDES, file)
        printinfo(2, "Compilation command is [%s]" % cmd)
        f=os.popen(cmd)
        out = f.read()
        status = f.close()
        if status and status & 0xff:
            error("compiler got signal %i while compiling %s" % (status&0xff,file), err=2)
        elif status:
            error("compiler reported error %i while compiling %s" % (status>>8,file), err=2)
        return out

    def assemble(self, file):
        printinfo(1, "** Assembling %s" % file)
        cmd = "%s %s %s -o %s.o %s" % (self.CC, self.LDFLAGS, self.ASSEMBLEFLAGS, file, file)
        printinfo(2, "Assembling command is [%s]" % cmd)
        status = os.system(cmd)
        if status and status & 0xff:
            error("compiler killed by signal %i while assembling %s" % (status & 0xff, file), err=3)
        elif status:
            error("compiler reported error %i while assembling %s" % (status >> 8, file), err=3)
        return self.extract_shcode(file+".o")
        

    def extract_shcode(self, file):
        printinfo(1,"** Extracting shellcode from %s" % file)
        cmd = "%s -j .text -s -z  %s" % (self.OBJDUMP, file)
        printinfo(2, "Extraction command is [%s]" % cmd)
        f = os.popen(cmd)
        inp = f.readlines()
        status = f.close()
        if status and status&0xff:
            error("objdump killed by signal %i while extracting from %s" % (status & 0xff, file), err=4)
        elif status:
            error("objdump reported error %i while extracting from %s" % (status >> 8, file), err=4)
            

        # Extract machine code
        dump=re.compile("^ [0-9a-f]{4}")
        out = []
        for l in inp:
            if dump.match(l):
                out += l[:42].split()[1:]
        out = "".join(out)
        shcode = ""
        for i in range(len(out)/2):
            shcode += chr(int(out[2*i:2*i+2],16))
        
        if not shcode:
            error("No code in .text section of %s !?"%file, err=2)
        return shcode
    
    def test(self, file):
        printinfo(1,"** Compiling test program %s" % file)
        cmd = "gcc -o %s.tst %s" % (file,file)
        printinfo(2, "Compilation command is [%s]" % cmd)
        status = os.system(cmd)
        if status and status &0xff:
            error("compiler killed by signal %i while compiling test program" % status & 0xff,err=5)
        elif status:
            error("compiler reported error %i while compiling test program" % status >> 8,err=5)
            
    def run(self, file):
        printinfo(1,"** Running test program %s" % file)
        abs=""
        if file[0] != "/":
            abs = "./"
        status=os.system(abs+file)
        if status & 0xff:
            printinfo(1, "\n** Test program killed by signal %i" % (status & 0xff))
        else:
            printinfo(1, "\n** Test program exited with exitcode=%i" % (status >> 8))
        return status


class Binutils_ldscript(Binutils):
    def __init__(self, *args, **kargs):
        Binutils.__init__(self, *args, **kargs)
        self.LDFLAGS +=" -nostdlib -nodefaultlibs -nostartfiles -Wl,-s,--gc-sections,-T,linker.ld,--oformat,binary "
        self.ASSEMBLEFLAGS = ""

    def extract_shcode(self, file):
        printinfo(1,"** Extracting shellcode from %s" % file)
        return open(file).read() 
