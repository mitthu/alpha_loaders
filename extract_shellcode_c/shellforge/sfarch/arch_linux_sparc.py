#############################################################################
##                                                                         ##
## arch_linux_parisc.py --- classes for Linux Sparc platform               ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: arch_linux_sparc.py,v 1.1 2003/10/19 17:50:42 pbi Exp $

from cpu_sparc import *

class Binutils_linux_sparc(Binutils_sparc):
    pass


class CodeTuner_linux_sparc(CodeTuner_gcc_sparc):
    pass


class Loaders_linux_sparc(Loaders_sparc):
    pass
