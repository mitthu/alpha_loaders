#############################################################################
##                                                                         ##
## arch_linux_mips.py --- classes for Linux/MIPS platform                  ##
##              see http://www.secdev.org/projects/shellforge.html         ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <phil@secdev.org>                   ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: arch_linux_mips.py,v 1.1 2004/04/14 19:48:20 pbi Exp $

from cpu_mips import *

class Binutils_linux_mips(Binutils_mips):
    pass


class CodeTuner_linux_mips(CodeTuner_gcc_mips):
    pass


class Loaders_linux_mips(Loaders_mips):
    pass
