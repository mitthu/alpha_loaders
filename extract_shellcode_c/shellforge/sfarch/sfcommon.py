#! /usr/bin/env python


#############################################################################
##                                                                         ##
## sfcommon.py --- shared functions and variables                          ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: sfcommon.py,v 1.6 2004/05/04 22:43:29 pbi Exp $

import sys,os,getopt
import warnings,string
from sfconfig import *

RCSID="$Id: sfcommon.py,v 1.6 2004/05/04 22:43:29 pbi Exp $"

#a=sys.argv[0]
#SYSTEM_INCLUDE=a[:a.rfind("/")]+"/include/"
#SYSTEM_INCLUDE="/home/pbi/work/prog/python/shellforge/sflib/sflib/"

warnings.filterwarnings("ignore","tempnam",RuntimeWarning, __name__)
TMPDIR=os.tempnam("/tmp","sf")+"/"
os.mkdir(TMPDIR)


def get_arch():
    f = os.popen("uname -s")
    kernel = f.read().strip().lower()
    if f.close():
        kernel = "linux"
    f = os.popen("uname -m")
    cpu = f.read().strip()
    if f.close():
        kernel = "i386"
    if cpu in ["i686","i586","i486"]:
        cpu = "i386"
    return "%s_%s" % (kernel, cpu)

class Input:
    c = 0
    asm = 1
    tunedasm = 2
    shcode = 3

class Output:
    c = 1
    raw = 2
    asm = 3
    hex = 4
    rawasm = 5

class Loader:
    none = 0
    xor = 1
    alpha = 2

class Options:
    verb = 0
    test = 0
    keep = 0
    arch = ""
    input = Input.c
    output = Output.hex
    loaders = []
    saveregs = 0
    stackreloc = 0
    src = ""
    use_errno = 0

Options.arch = get_arch()


def printinfo(lvl, x):
    if lvl <= Options.verb:
        os.write(2,x+"\n")

def error(x,err=1,printusage=0):
    printinfo(0,"ERROR: %s" % x)
    if printusage:
        usage()
    sys.exit(err)

def usage():
    printinfo(0, """Usage: shellforge [-v <verb>] [-a] [-C|-R|-A] [-t[t]] [-k] [--in=<inform> 
--out=<outform> --arch=<target arch> <src file>
--in=<x>   : input form, x is one of %s
  -a       : input is assembler
--out=<x>  : output form, x is one of %s
  -C       : output is a C file
  -R       : output is the raw shellcode
  -A       : output is assembler
  -t       : compile the C output
  -tt      : compile and run the C output
  -k       : does not remove the temporary directory
  -v<x>    : change the verbosity, from 0 to 5
--arch=<x> : target architecture, x is one of %s
--loader=<x> : add a loader. Loaders are added in the order they
               appear in the command line. They can take parameters :
               --loader=loadername:opt1=val1,val2,val3:opt2=val4,val5
Example:
shellforge.py -C --arch=freebsd_i386 --loader=stackreloc \\
                 --loader=xor:avoid=0x00,0x0a hello.c
""")





    
    
        
def parse_parameters(argv):
    try:
        opts=getopt.getopt(argv, "v:eVaACRthkxsS",["in=","out=","loader=","arch="])
        for opt,optarg in opts[0]:
            if opt == "-h":
                usage()
                sys.exit(0)
            if opt == "-V":
                printinfo(0, "ShellForge v%s\nCopyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>" % VERSION)
                sys.exit(0)
            elif opt == "-v":
                try:
                    Options.verb = int(optarg)
                except ValueError,msg:
                    raise getopt.GetoptError(str(msg),None)
            elif opt == "-e":
                Options.use_errno = 1
            elif opt == "-a":
                Options.input = Input.asm
            elif opt == "-C":
                Options.output = Output.c
            elif opt == "-A":
                Options.output = Output.asm
            elif opt == "-R":
                Options.output = Output.raw
            elif opt == "-x":
                Options.loaders += ["xor"]
            elif opt == "-t":
                Options.test += 1
            elif opt == "-k":
                Options.keep += 1
            elif opt == "--in":
                Options.input = getattr(Input, optarg.lower())
            elif opt == "--out":
                Options.output = getattr(Output, optarg.lower())
            elif opt == "--loader":
                opt = {}
                loader,options = (optarg.split(":",1)+[None])[:2]
                if options:
                    for o in options.split(":"):
                        n,v = (o.split("=",1)+[None])[:2]
                        opt[n] = v
                Options.loaders += [(loader.lower(),opt)]
            elif opt == "--arch":
                Options.arch = optarg.translate(string.maketrans("-","_"))
        if len(opts[1]) > 1:
            raise getopt.GetoptError("too many paramters after options.",None)
        elif len(opts[1]) == 0:
            raise getopt.GetoptError("source file missing.",None)
        Options.src = opts[1][0]
    except getopt.GetoptError,msg:
        error(msg, printusage=1)
    except SystemExit:
        sys.exit(0);
    #except:
    #    error("parsing arguments", printusage=1);
    
    



class Shellcode:
    
    def __init__(self, binutils, code_tuner, tmpdir=TMPDIR):
        self.binutils = binutils
        self.code_tuner = code_tuner

        self.tunedasm = None
        self.shcode = None
        self.inputpoint = { Input.c        :  self.readfile_C,
                            Input.asm      :  self.readfile_asm,
                            Input.tunedasm :  self.readfile_tunedasm,
                            Input.shcode   :  self.readfile_shcode, }
        self.outputpoint = { Output.c    : self.write_C,
                             Output.raw  : self.write_raw,
                             Output.rawasm  : self.write_rawasm,
                             Output.hex  : self.write_hex,
                             Output.asm  : self.write_tunedasm, }

    def readfile_C(self, source):
        self.read_asm(self.Cfile_to_asm(source))
    def readfile_asm(self, source):
        f = open(source, "r")
        self.read_asm(f.read())
        f.close()
    def readfile_tunedasm(self, source):
        f = open(source, "r")
        self.read_tunedasm(f.read())
        f.close()
    def readfile_shcode(self, source):
        f = open(source, "r")
        self.shcode = f.read()
        f.close()

    def read_C(self, input):
        name = TMPDIR+"input.c"
        f = open(name,"w")
        f.write(input)
        f.close()
        self.readfile_C(name)
    def read_asm(self, input):
        self.asm = input
    def read_tunedasm(self, input):
        self.tunedasm = input

    def write_rawasm(self):
        return self.asm

    def write_raw(self):
        self.ensure_assembled()
        return self.shcode
    def write_hex(self):
        self.ensure_assembled()
        return "".join(map(lambda x: "\\x%02x" % ord(x), self.shcode))
    def write_C(self):
        hexa = self.write_hex()
        ret = 'unsigned char shellcode[] = \n'
        for i in range(0, len(hexa), 76):
            ret += '"%s"\n' % hexa[i:i+76]
        ret += ";int main(void) { ((void (*)())shellcode)(); }\n"
        return ret
    def write_tunedasm(self):
        self.ensure_tuned()
        return self.tunedasm


    def ensure_tuned(self):
        if self.tunedasm is None:
            self.tunedasm = self.asm_to_tunedasm(self.asm)
    def ensure_assembled(self):
        self.ensure_tuned()
        if self.shcode is None:
            self.assemble()
    def assemble(self):
        name = TMPDIR + "tunedasm.s"
        f = open(name, "w")
        f.write(self.tunedasm)
        f.close()
        self.shcode = self.binutils.assemble(name)
    def Cfile_to_asm(self, source):
        return self.binutils.compile(source)
    def asm_to_tunedasm(self, input):
        return self.code_tuner.tune_code(input)
    def apply_loader(self, loader, options):
        self.ensure_assembled()
        self.shcode = loader(self.shcode, **options)
