#############################################################################
##                                                                         ##
## arch_linux_alpha.py --- classes for linux_alpha platform                      ##
##              see http://www.secdev.org/projects/shellforge.html         ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <phil@secdev.org>                   ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: arch_linux_alpha.py,v 1.1 2003/12/08 13:52:06 pbi Exp $

from cpu_alpha import *

class Binutils_linux_alpha(Binutils_alpha):
    pass


class CodeTuner_linux_alpha(CodeTuner_gcc_alpha):
    pass


class Loaders_linux_alpha(Loaders_alpha):
    pass
