#############################################################################
##                                                                         ##
## arch_linux_parisc.py --- classes for Linux PARISC platform              ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: arch_linux_hppa.py,v 1.1 2003/10/19 17:50:42 pbi Exp $

from cpu_hppa import *

class Binutils_linux_hppa(Binutils_hppa):
    pass


class CodeTuner_linux_hppa(CodeTuner_gcc_hppa):
    pass


class Loaders_linux_hppa(Loaders_hppa):
    pass
