#############################################################################
##                                                                         ##
## arch_freebsd_i386.py --- classes for FreeBSD i386 platform              ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: arch_freebsd_i386.py,v 1.1.1.1 2003/09/06 22:02:57 biondi Exp $

from cpu_i386 import *

class Binutils_freebsd_i386(Binutils_i386):
    pass


class CodeTuner_freebsd_i386(CodeTuner_gcc_i386):
    pass

class Loaders_freebsd_i386(Loaders_i386):
    pass
