#############################################################################
##                                                                         ##
## arch_linux_mipsel.py --- classes for Linux/MIPSel platform              ##
##              see http://www.secdev.org/projects/shellforge.html         ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <phil@secdev.org>                   ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: arch_linux_mipsel.py,v 1.1 2004/05/04 17:02:28 pbi Exp $

from cpu_mips import *

class Binutils_linux_mipsel(Binutils_mips):
    pass


class CodeTuner_linux_mipsel(CodeTuner_gcc_mips):
    pass


class Loaders_linux_mipsel(Loaders_mips):
    pass
