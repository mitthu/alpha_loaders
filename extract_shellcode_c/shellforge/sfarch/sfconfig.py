#############################################################################
##                                                                         ##
## sfconfig.py --- This file replace a configuration file                  ##
##              see http://www.secdev.org/projects/shellforge.html         ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2004  Philippe Biondi <phil@secdev.org>                   ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: sfconfig.py,v 1.3 2004/05/20 15:10:16 pbi Exp $


# This file stands for a configuration file

VERSION = "0.2.0pre6"

SYSTEM_INCLUDE="../sflib/sflib/"

# This is used to know which gcc and objdump to use
# The key can be "targetArch", "hostOS:targetArch", "hostArch:targetArch"
# The more precise is choosen

compilers = { "linux:linux_hppa" : ("hppa-linux-gcc", "hppa-linux-objdump"),
              "linux:linux_sparc": ("sparc-linux-gcc", "sparc-linux-objdump"),
              "linux:solaris_sparc": ("sparc-linux-gcc", "sparc-linux-objdump"),
              "linux:linux_arm": ("arm-linux-gcc", "arm-linux-objdump"),
              "linux:linux_alpha": ("alpha-linux-gcc", "alpha-linux-objdump"),
              "linux:linux_powerpc": ("powerpc-linux-gcc", "powerpc-linux-objdump"),
              "linux:macos_powerpc": ("powerpc-linux-gcc", "powerpc-linux-objdump"),
              "linux:linux_mips": ("mips-linux-gcc", "mips-linux-objdump"),
              "linux:linux_mipsel": ("mipsel-linux-gcc", "mipsel-linux-objdump"),
              "linux:linux_m68k": ("m68k-linux-gcc", "m68k-linux-objdump"),
              }


