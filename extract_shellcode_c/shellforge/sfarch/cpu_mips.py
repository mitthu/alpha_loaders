#############################################################################
##                                                                         ##
## cpu_mips.py --- Classes for MIPS CPUs                                   ##
##              see http://www.secdev.org/projects/shellforge.html         ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: cpu_mips.py,v 1.2 2004/04/30 10:12:48 pbi Exp $

from cpu_any import *


class Loaders_mips(Loaders):
    def loader_xor(self, shcode):
        key = self.opts.get("xorkey", None)
        if key is None:
            if "\x00" not in shcode:
                return shcode
            ld=""
            for i in range(1,256):
                if chr(i) not in shcode:
                    key=i
                    break
            if key is None:
                error("xor loader : no suitable xor key found.")
        printinfo(1,"** Applying xor loader. key=%#02x" % key)
        shcode = "".join(map(lambda x: chr(ord(x)^key), shcode))
        length = len(shcode)
        if length < 0x100:
            ld = ("\xeb\x0d\x5e\x31\xc9\xb1"+chr(length)+"\x80\x36"+
                  chr(key)+"\x46\xe2\xfa\xeb\x05\xe8\xee\xff\xff\xff")
        else:
            if length & 0xff == 0:
                length += 1
            ld = ("\xeb\x0f\x5e\x31\xc9\x66\xb9"+
                  chr(length&0xff)+chr(length>>8)+
                  "\x80\x36"+chr(key)+
                  "\x46\xe2\xfa\xeb\x05\xe8\xec\xff\xff\xff")
        return ld+shcode



class CodeTuner_gcc_mips(CodeTuner):
    def __init__(self, stack_reloc = 0, save_regs = 0):
        CodeTuner.__init__(self)
        self.stack_reloc = stack_reloc
        self.save_regs = save_regs
        
    def tune_code(self, code):
        printinfo(1,"** Tuning assembler code")
        codelines = code.splitlines()
        preamb = []
        rodata = []
        textpream = []
        text = []
        end = []
        
        out = [["# Modified by shellforge v%s\n"%VERSION]]

        state=0
        for l in codelines:
            printinfo(4, "[%i] %s"% (state, l))
            if l.find("@PLT") >= 0:
                error("Error at [%s]: Symbol not found" % (l.strip()), err=2)
            if state == 0:
                if l.find(".rdata") >= 0:
                    state = 1
                    continue
                elif l.find(".text") >= 0:
                    state = 2
                else:
                    preamb.append(l);
            if state == 1:
                if l.find(".text") >= 0:
                    state = 2
                else:
                    rodata.append(l)
            if state == 2:
                if l.find("main:") >= 0:
                    state = 3
                else:
                    textpream.append(l)
            if state == 3:
                if l.find(".end") >= 0:
                    state = 4
                else:
                    text.append(l)
            if state == 4:
                end.append(l)
                
        if state != 4:
            self.automaton_error()
    
        out += [preamb, textpream, text]
                
    
        out += [rodata, end]

        out = reduce(lambda x,y: x+["#---------"]+y, out)

        return "\n".join(out)+"\n"


class Binutils_mips(Binutils):
    def __init__(self, *args, **kargs):
        Binutils.__init__(self, *args, **kargs)

class Binutils_mips_test(Binutils_ldscript):
    def __init__(self, *args, **kargs):
        Binutils_ldscript.__init__(self, *args, **kargs)
        self.LDFLAGS = self.LDFLAGS.replace("linker.ld","linker-mipsel.ld")
        self.CFLAGS  += ""
