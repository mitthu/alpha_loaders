#############################################################################
##                                                                         ##
## cpu_arm.py --- Classes for Advanced RISC Machines                       ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: cpu_arm.py,v 1.4 2004/05/20 15:10:16 pbi Exp $

from cpu_any import *


class Loaders_arm(Loaders):
    pass

    def loader_xor(self, shcode):
        error("ARM xor loader still not working... sorry.")
        key = self.opts.get("xorkey", None)
        if key is None:
            if "\x00" not in shcode:
                return shcode
            ld=""
            for i in range(1,256):
                if chr(i) not in shcode:
                    key=i
                    break
            if key is None:
                error("xor loader : no suitable xor key found.")
        printinfo(1,"** Applying xor loader. key=%#02x" % key)
#        shcode = "".join(map(lambda x: chr(ord(x)^key), shcode))
        length = len(shcode)
        if length < 0x100:
            ld = (
                "\x28\x40\x8f\xe2"+         # add     r4, pc, #40     ; 0x28
                  "\x04\x10\xa0\xe1"+         # mov     r1, r4
                  chr(length)+"\x60\xa0\xe3"+ # mov     r6, LENGTH
                  "\x06\x20\xa0\xe1"+         # mov     r2, r6
                  "\x03\x30\x23\xe0"+         # eor     r3, r3, r3
                  "\x03\x50\xd4\xe6"+         # ldrb    r5, [r4], r3
                  chr(key)+"\x50\x25\xe2"+    # eor     r5, r5, KEY
                  "\x01\x50\xc4\xe4"+         # strb    r5, [r4], #1
                  "\x01\x60\x56\xe2"+         # subs    r6, r6, #1      ; 0x1
                  "\xfa\xff\xff\x1a"+         # bne     8514 <main+0x14>
                  "\x00\x00\xa0\xe1"+ # nop
#                  "\x02\x30\x40\xe5"+         # strb    r3, [r0, -#2]   ;  /* change next isn to "swi 0x009f0002" */
                  "\x00\x00\xa0\xe1" # nop                  
#                  "\x02\x40\x9f\xef"
                  )         # swi     0x009f4002      ;  /* flushes I-cache */

        else:
            error("length=%i > 255, TODO XXX"%length);

        return ld+shcode




class CodeTuner_gcc_arm(CodeTuner):
    def __init__(self, stack_reloc = 0, save_regs = 0):
        CodeTuner.__init__(self)
        self.stack_reloc = stack_reloc
        self.save_regs = save_regs
        if stack_reloc or save_regs:
            error("Stack relocation and register saving not supported yet for this arch.\n")
            
        
        
    def tune_code(self, code):
        printinfo(1,"** Tuning assembler code")
        codelines = code.splitlines()
        preamb = []
        rodata = []
        textbegin = []
        gotaddr = [ ".LICI:", "\tadd\tsl, pc, #main-.LICI-8" ]
        textend = []
        end = []
        
        out = [["# Modified by shellforge v%s\n"%VERSION]]
        state=0
        for l in codelines:
            printinfo(4, "[%i] %s"% (state, l))
            if l.find("@PLT") >= 0:
                error("Error at [%s]: Symbol not found" % (l.strip()), err=2)

            if l.find("ldr") >= 0 and l.find("[sl,") >= 0:
                ll = l.split()
                l = "\tadd\t"+ll[1]+" sl, "+ll[3][:-1]
            if l.find("(GOT)") >= 0:
                      l = l[:l.find("(GOT)")]+"-main"
            if l.find("ldr") >= 0 and l.find("sl") >= 0 and l.find(".L") >= 0:
                continue

            if state == 0:
                if l.find(".rodata") >= 0:
                    state = 1
                    continue
                elif l.find(".text") >= 0:
                    state = 2
                else:
                    preamb.append(l);
            if state == 1:
                if l.find(".text") >= 0:
                    state = 2
                else:
                    rodata.append(l)
            if state == 2:
                if l.find("add") >= 0 and l.find("sl, pc, sl") >= 0:
                    state = 3
                    continue
                textbegin.append(l)                
            if state == 3:
                if (l.find(".Lfe1:") >= 0 or 
                    (l.find(".size") >= 0 and l.find("main") >= 0)):
                    state = 4
                else:
                    textend.append(l)
            if state == 4:
                end.append(l)
        
        if state != 4:
            self.automaton_error()

        out += [preamb, textbegin, gotaddr, textend, rodata, end ]

        out = reduce(lambda x,y: x+["#---------"]+y, out)
        return "\n".join(out)+"\n"


class Binutils_arm(Binutils):
    def __init__(self, *args, **kargs):
        Binutils.__init__(self, *args, **kargs)
        self.ASSEMBLEFLAGS = "-nostartfiles -nostdlib -shared"


#        .text
#        .align  2
#        .global main
#        .type   main,function
#main:
#        @ args = 0, pretend = 0, frame = 0
#        @ frame_needed = 0, uses_anonymous_args = 0
#        add     r4, pc, #.Lshellcode-main-8
#        mov     r1, r4
#        mov     r6, LENGTH
#        mov     r2, r6
#        eor     r3, r3, r3
#.Lloop:
#        ldrb    r5, [r4], r3
#        eor     r5, r5, XORKEY
#        strb    r5, [r4], #1
#        subs    r6, r6, #1
#        bne     .Lloop          /* will not work. Use 1afffffa */
#        strb    r3, [r0,#-2]
#        swi     0x9f4002        /* flushes I-cache */
#.Lshellcode:
#

