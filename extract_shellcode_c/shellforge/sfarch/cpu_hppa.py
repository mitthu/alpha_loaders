#############################################################################
##                                                                         ##
## cpu_parisc.py --- Classes for PA-RISC CPUs                              ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: cpu_hppa.py,v 1.3 2003/11/07 11:14:56 pbi Exp $

from cpu_any import *


class Loaders_hppa(Loaders):
    pass

#    options = Loaders.options+["xorkey"]
#    def loader_xor(self, shcode):
#        key = self.opts.get("xorkey", None)
#        if key is None:
#            if "\x00" not in shcode:
#                return shcode
#            ld=""
#            for i in range(1,256):
#                if chr(i) not in shcode:
#                    key=i
#                    break
#            if key is None:
#                error("xor loader : no suitable xor key found.")
#        printinfo(1,"** Applying xor loader. key=%#02x" % key)
#        shcode = "".join(map(lambda x: chr(ord(x)^key), shcode))
#        length = len(shcode)
#        if length < 0x100:
#            ld = ("\xeb\x0d\x5e\x31\xc9\xb1"+chr(length)+"\x80\x36"+
#                  chr(key)+"\x46\xe2\xfa\xeb\x05\xe8\xee\xff\xff\xff")
#        else:
#            if length & 0xff == 0:
#                length += 1
#            ld = ("\xeb\x0f\x5e\x31\xc9\x66\xb9"+
#                  chr(length&0xff)+chr(length>>8)+
#                  "\x80\x36"+chr(key)+
#                  "\x46\xe2\xfa\xeb\x05\xe8\xec\xff\xff\xff")
#        return ld+shcode
#



class CodeTuner_gcc_hppa(CodeTuner):
    def __init__(self, stack_reloc = 0, save_regs = 0):
        CodeTuner.__init__(self)
        self.stack_reloc = stack_reloc
        self.save_regs = save_regs
        if stack_reloc or save_regs:
            error("Stack relocation and register saving not supported yet for this arch.\n")
            
        
        
    def tune_code(self, code):
        printinfo(1,"** Tuning assembler code")
        codelines = code.splitlines()
        preamb = []
        rodata = []
        begintext = []
#        got = [ "\tblr %r0, %r20" ]
        got = [ ]
        endtext = []
        end = []
        
        out = [["# Modified by shellforge v%s\n"%VERSION]]
        state=0
        for l in codelines:
            printinfo(4, "[%i] %s"% (state, l))
            if l.find("@PLT") >= 0:
                error("Error at [%s]: Symbol not found" % (l.strip()), err=2)

            #"addil LT'.LC0,%r19" ==> "b,l  0, %r1"
            if l.find("addil") >= 0 and l.find("LT'") >= 1 and l.find("r19") >= 0:
                var = l[l.find("LT'")+3:l.find(",%")]
                l = ".LLL%s:\n\tb,l 0, %%r1" % var
            #"ldw RT'.LC0(%r1),%r20" ==>  "ldo .LC0-main-11(%r1),%r20"
            if l.find("ldw") >= 0 and l.find("RT'") >= 0 and l.find("%r1") >= 0:
                var = l[l.find("RT'")+3:l.find("(%")]
                l = "\tldo %s-.LLL%s-11(%%r1),%s" % (var,var,
                                                  l[l.find(",")+1:])


                
            if state == 0:
                if l.find(".rodata") >= 0:
                    state = 1
                    continue
                elif l.find(".text") >= 0:
                    state = 2
                else:
                    preamb.append(l);
            if state == 1:
                if l.find(".text") >= 0:
                    state = 2
                else:
                    rodata.append(l)
            if state == 2:
                state = 3
#                if l.find("ldw") >= 0 and l.find("RT'") >= 1 and l.find("(%r1)"):
#                    state = 3
#                    continue
#                begintext.append(l)
            if state == 3:
                if (l.find(".Lfe1:") >= 0 or 
                    (l.find(".size") >= 0 and l.find("main") >= 0)):
                    state = 4
                else:
                    endtext.append(l)
            if state == 4:
                end.append(l)
        
        if state != 4:
            self.automaton_error()

        out += [preamb,begintext,got,endtext,rodata,end]
        out = reduce(lambda x,y: x+["#---------"]+y, out)
        return "\n".join(out)+"\n"


class Binutils_hppa(Binutils):
    pass
        
