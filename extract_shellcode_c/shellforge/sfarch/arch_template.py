#############################################################################
##                                                                         ##
## arch_template.py --- classes for template platform                      ##
##              see http://www.secdev.org/projects/shellforge.html         ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <phil@secdev.org>                   ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: arch_template.py,v 1.2 2004/04/14 19:52:19 pbi Exp $

from cpu_tempcpu import *

class Binutils_template(Binutils_tempcpu):
    pass


class CodeTuner_template(CodeTuner_gcc_tempcpu):
    pass


class Loaders_template(Loaders_tempcpu):
    pass
