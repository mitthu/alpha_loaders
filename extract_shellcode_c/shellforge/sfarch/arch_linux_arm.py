#############################################################################
##                                                                         ##
## arch_linux_arm.py --- classes for Linux on ARM                          ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: arch_linux_arm.py,v 1.1 2003/11/01 22:48:25 pbi Exp $

from cpu_arm import *

class Binutils_linux_arm(Binutils_arm):
    pass


class CodeTuner_linux_arm(CodeTuner_gcc_arm):
    pass


class Loaders_linux_arm(Loaders_arm):
    pass
