#! /usr/bin/env python


#############################################################################
##                                                                         ##
## shellforge.py --- C to shellcode conversion programm                    ##
##              see http://www.cartel-securite.net/pbiondi/shellforge.html ##
##              for more informations                                      ##
##                                                                         ##
## Copyright (C) 2003  Philippe Biondi <biondi@cartel-securite.fr>         ##
##                                                                         ##
## This program is free software; you can redistribute it and/or modify it ##
## under the terms of the GNU General Public License as published by the   ##
## Free Software Foundation; either version 2, or (at your option) any     ##
## later version.                                                          ##
##                                                                         ##
## This program is distributed in the hope that it will be useful, but     ##
## WITHOUT ANY WARRANTY; without even the implied warranty of              ##
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       ##
## General Public License for more details.                                ##
##                                                                         ##
#############################################################################

# $Id: shellforge.py,v 1.7 2004/05/04 22:17:11 pbi Exp $

import os,sys
from sfarch.sfcommon import *
from sfarch.arch_linux_i386 import *
from sfarch.arch_freebsd_i386 import *
from sfarch.arch_openbsd_i386 import *
from sfarch.arch_linux_hppa import *
from sfarch.arch_linux_sparc import *
from sfarch.arch_linux_arm import *
from sfarch.arch_linux_alpha import *
from sfarch.arch_linux_mips import *
from sfarch.arch_linux_mipsel import *
from sfarch.arch_linux_powerpc import *
from sfarch.arch_solaris_sparc import *
from sfarch.arch_macos_powerpc import *



parse_parameters(sys.argv[1:])

host_arch = get_arch()
printinfo(1,"** Running shellforge v%s" % VERSION)
printinfo(1,"** Host arch is %s" % host_arch)
printinfo(1,"** ARCH=%s StackReloc=%i SaveRegs=%i" % (Options.arch,
                                                      Options.stackreloc,
                                                      Options.saveregs))



k = ":".join((host_arch,Options.arch))
if compilers.has_key(k):
    Options.cc, Options.objdump = compilers[k]
else:
    k = ":".join((host_arch.split("_")[0],Options.arch))
    if compilers.has_key(k):
        Options.cc, Options.objdump = compilers[k]
    else:
        Options.cc, Options.objdump = ("gcc","objdump")

printinfo(1,"** CC=%s OBJDUMP=%s" % (Options.cc,Options.objdump))
    
    
CFLAGS = ""
if Options.use_errno:
    CFLAGS += " -DSF_USE_ERRNO"

try:
    the_binutils = locals()["Binutils_%s"%Options.arch](Options.arch, SYSTEM_INCLUDE, CC=Options.cc, OBJDUMP=Options.objdump, CFLAGS=CFLAGS)
except KeyError:
    error("No binutils available in shellforge for arch [%s]" % Options.arch)
    
if Options.verb >= 5:
    printinfo(5,"** Context")
    the_binutils.show_versions()


try:
    the_code_tuner = locals()["CodeTuner_%s" % Options.arch](Options.stackreloc, Options.saveregs)
except KeyError:
    error("No code tuner available in shellforge for arch [%s]" % Options.arch)
    
try:
    the_loaders = locals()["Loaders_%s" % Options.arch]()
except KeyError:
    error("No loaders available in shellforge for arch [%s]" % Options.arch)
    

    
the_shcode = Shellcode(the_binutils, the_code_tuner)

the_shcode.inputpoint[Options.input](Options.src)

for ldr,opt in Options.loaders:
    try:
        the_shcode.apply_loader(getattr(the_loaders,"loader_%s" % ldr),opt)
    except AttributeError:
        error("Loader [%s] for arch [%s] not found" % (i,Options.arch))

    


if Options.test == 0:
    print the_shcode.outputpoint[Options.output]()
else:
    shcode = the_shcode.outputpoint[Output.c]()
    testfile = TMPDIR+"shcode.c"
    f = open(testfile,"w")
    f.write(shcode)
    f.close()
    the_binutils.test(testfile)
    if Options.test > 1:
        the_binutils.run(testfile+".tst")

if Options.keep == 0:
    printinfo(1, "** Removing temporary directory %s" % TMPDIR)
    os.system("rm -rf '%s'"%TMPDIR)


