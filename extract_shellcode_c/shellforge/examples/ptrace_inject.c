#include <sys/user.h>
#define ERESTARTSYS     512
#define ERESTARTNOINTR  513
#define ERESTARTNOHAND  514     /* restart if no handler.. */
#define   WUNTRACED       2       /* Report status of stopped children.  */



#define STR "Hello world!\n"

// extern int SHCODE_END;

//#define LOADSZ ((int)(&loadend)-(int)(&load))
#define LOADSZ 700

static int load(void)
{
	__asm__("pusha");
//	__asm__("int $3");
	write(1,STR,sizeof(STR));
	__asm__("popa");
}


//static void loadend(void) {}

int main(void)
{
	int pid, old_eip,start,i;
        struct user_regs_struct regs;

	pid = getppid();
        ptrace(PTRACE_ATTACH, pid, NULL, NULL);
        waitpid(pid, 0, WUNTRACED);
	ptrace(PTRACE_GETREGS, pid, NULL, &regs);	
        start = regs.esp-512-LOADSZ;
        for (i=0; i < LOADSZ; i+=4) 
                ptrace(PTRACE_POKEDATA, pid, (void *)(start+i), (void *)*(int *)(((unsigned char *)(&load))+i) );

        /**** Change execution flow ****/
        old_eip = regs.eip;
        regs.eip = start;
        if ( (regs.orig_eax >= 0) &&
             (regs.eax == -ERESTARTNOHAND ||
              regs.eax == -ERESTARTSYS ||
              regs.eax == -ERESTARTNOINTR) ) {
                regs.eip += 2;
                old_eip -= 2;
        }

        /** push eip ****/
        regs.esp -= 4;
        ptrace(PTRACE_POKEDATA, pid, (char *)regs.esp, (char *)old_eip);

        ptrace(PTRACE_SETREGS, pid, NULL, &regs);
        ptrace(PTRACE_DETACH, pid, NULL, NULL);
	exit(-1);
}

	
	
