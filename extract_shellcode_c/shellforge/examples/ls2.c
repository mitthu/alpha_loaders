#include "include/sfsyscall.h"

int main(void)
{
	int fd;
	struct dirent d;

	write(1,"go[",4);

	fd = open("/", O_RDONLY, 0);
	while (readdir(fd, &d, 1)) {
		write(1, d.d_name, d.d_reclen);
	}
	write(1,"]og",4);

	exit(0);
}
