
// #define SF_USE_ERRNO

int main(void) 
{
	if (open("/donotexist/prettysureofthat",O_RDONLY,0) == -1) {
		write(1,"nook",4);
		write(1,&errno,4);
	}
	else
		write(1,"Ok.\n",4);

	exit(0);
}
