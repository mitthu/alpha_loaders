/*
 * sflib.h --- SFLib syscall library for HPUX/PA-RISC 
 * see http://www.secdev.org/projects/shellforge.html for more informations
 *
 * Copyright (C) 2004  Philippe Biondi <phil@secdev.org>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */


/*
 * Automatically generated by gensflib.py 
 * Sat Apr 19 12:26:26 2014
 */

#ifndef SFLIB_H
#define SFLIB_H


#include "sfsysnr.h"
#include "sfsyscall.h"
#include "../common/sftypes.h"
        

#endif /* SFLIB_H */
