HEADER_NAT = header_nat
HEADER_AF = header_af
SHELL_C = extract_shellcode_c
SHELL_ASM_NASM = extract_shellcode_asm_nasm
SHELL_ASM_GAS = extract_shellcode_asm_gas

.PHONY: compile
compile:
	$(MAKE) -C $(HEADER_NAT)
	$(MAKE) -C $(HEADER_AF)
	$(MAKE) -C $(SHELL_C)
	$(MAKE) -C $(SHELL_ASM_NASM)
	$(MAKE) -C $(SHELL_ASM_GAS)

.PHONY: clean
clean:
	$(MAKE) -C $(HEADER_NAT) clean
	$(MAKE) -C $(HEADER_AF) clean
	$(MAKE) -C $(SHELL_C) clean
	$(MAKE) -C $(SHELL_ASM_NASM) clean
	$(MAKE) -C $(SHELL_ASM_GAS) clean
