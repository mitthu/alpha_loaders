#!/bin/bash

export $1

envsubst <<EOF
//	Created by mitthu_asc on `date`.
//	Copyright (c) 2014 Aditya Basu. All rights reserved.
//	Compile using:
//		gcc -fno-stack-protector -m32 -z execstack shellcode.c -o shellcode

#include <stdio.h>
#include <string.h>

char shellcode[] = "$1";

int main(int argc, char **argv) {
	char buffer[sizeof(shellcode)];
	strcpy(buffer, shellcode);
	
	asm("movl -4(%ebp), %eax");
	(*(void(*)()) buffer)();
}

EOF
