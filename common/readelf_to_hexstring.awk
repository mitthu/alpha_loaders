#!/usr/bin/awk -f
# Input from command:
# objdump -d -j .text <asm>
# Input format:
# 55 89 e5 83 ec 08 b8 00 00 00 00 c9 c3
# Output Format:
# \\x55\\x89\\xe5\\x83\\xec\\x08\\xb8\\x00\\x00\\x00\\x00\\xc9\\xc3

{ 
    if (NR != 1 && NR != 2) {
	    for (i=2; i <= NF-1; i++) {
    	    printf $i;
   		}
   	}
}

END {
}
