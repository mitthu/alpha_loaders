#!/bin/bash

envsubst <<EOF
# Created by mitthu_asc on `date`.
# Copyright (c) 2014 Aditya Basu. All rights reserved.
# Use:
#  - Compiling the shellcode.c file.

shellcode: shellcode.c
	gcc -fno-stack-protector -m32 -z execstack shellcode.c -o shellcode
	
clean:
	rm -f shellcode

EOF
