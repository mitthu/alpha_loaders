#!/usr/bin/python
import sys

if '-c' in sys.argv or '--c-hex-string' in sys.argv:
	raw = sys.stdin.read()
	raw = raw.replace('\\x', '')[:-1]
	raw = raw.decode("hex")
	sys.stdout.write(raw)
	exit

raw = sys.stdin.read(2)
while raw != '':
	try:
		raw = chr(int(raw, 16))
	except:
		break
	sys.stdout.write(raw)
	raw = sys.stdin.read(2)

